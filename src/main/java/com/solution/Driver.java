package main.java.com.solution;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/*
    This Driver class can be used to perform a word search given a (correctly formatted) input file
 */
public class Driver {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        //get the file path from the user
        System.out.print("Enter the path to the file: ");
        String path = scan.next();

        //prints the results of the search
        System.out.println(runOnFile(new File(path)));
    }
    /*
        Takes in a file with a word search grid size, the grid itself, and words to find
        Reads the contents of the file and passes the pertinent data to findWords()
        Returns a string of results (if any) consisting of the found words and their starting/ending positions in the grid
    */
    public static String runOnFile(File file) {
        //create a BufferedReader help read file
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        //stores the size of the grid
        int rowCount = 0, colCount = 0;
        //keeps track of where we are in the file
        int lineCount = 0;
        //initialize the grid to be searched
        char[][] grid = null;
        //instantiate the list of words to search for
        List<String> words = new ArrayList<>();
        //initialize the string object that will store the current line
        String st = null;

        while (true) {
            try {
                if ((st = br.readLine()) == null) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            //the first line will always be the size formatted as <row>x<col> so lets grab those
            if (lineCount == 0) {
                rowCount = Integer.parseInt(String.valueOf(st.charAt(0)));
                colCount = Integer.parseInt(String.valueOf(st.charAt(2)));
            }
            //we know we're at the grid data lines if the lineCount is more than 0 and <= the row size
            else if (lineCount <= rowCount) {
                //the second line is where the grid starts so lets set the size
                if (lineCount == 1) {
                    grid = new char[rowCount][colCount];
                }
                grid[lineCount - 1] = st.replace(" ", "").toCharArray();
            }
            //if the lineCount is larger than the row size then we've hit the list of words
            else {
                words.add(st.replace(" ", ""));
            }
            lineCount++;
        }

        //pass the grid and target words to start searching
        return findWords(grid, words);
    }

    /*
        Accepts the grid and the list of words to search for
        Creates a list of directions and iterates through the word list, calling findWord() on each one
        Returns a string of results
    */
    private static String findWords(char[][] grid, List<String> words) {

        //we are going to use StringBuilder as it is more efficient than "mutating" Strings and
        //the time complexity can get pretty big with larger grids
        StringBuilder results = new StringBuilder();

        //build a list of directions we will travel at each position, if need be
        Direction[] directions = new Direction[]{
                new Direction(-1, -1),
                new Direction(-1, 0),
                new Direction(-1, 1),
                new Direction(1, -1),
                new Direction(1, 0),
                new Direction(1, 1),
                new Direction(0, -1),
                new Direction(0, 1),
        };

        //if there are no words to search, there will be no results!
        if(words.size() < 1) {
            return "";
        }

        //for each word, perform a search
        for (String word : words) {
            StringBuilder result = findWord(grid, word, directions);
            if(result.length() > 0) {
                results.append(word).append(result).append("\n");
            }
        }

        //if we get some results, return them
        if(results.length() > 0) {
            //we have to get rid of the empty line at the end
            return results.toString().substring(0, results.lastIndexOf("\n"));
        }

        //if no results, return an empty string
        return "";
    }

    /*
        Takes in a file with a word search grid size, the grid itself, and words to find
        Goes through each position of the grid and calls traversePosition() if the first letter of the target word is found
        Returns a string of results (if any) consisting of the found words and their starting/ending positions in the grid
    */
    private static StringBuilder findWord(char[][] grid, String word, Direction[] directions) {
        StringBuilder foundPositions = new StringBuilder();
        for (int x = 0; x < grid.length; x++) {
            for (int y = 0; y < grid[x].length; y++) {

                //if the character at this position doesn't match, no need to traverse
                if (grid[x][y] != word.charAt(0)) {
                    continue;
                }

                //if the word is only one letter long and it matches, just return the current position
                if(word.length() == 1) {
                    foundPositions.append(" ").append(x).append(":").append(y).append(" ").append(x).append(":").append(y);
                    continue;
                }

                //traverse this position in all directions
                foundPositions.append(traversePosition(grid, word, x, y, directions));
            }
        }
        //return the positions if there are any
        return foundPositions;
    }

    /*
        Accepts the search grid, the target word, the x and y coordinates of the position to traverse, and a list of directions
        Travels in each direction from the starting position trying to find the word in each direction
        Returns a StringBuilder of found positions
    */
    static private StringBuilder traversePosition(char[][] grid, String word, int x, int y, Direction[] directions) {
        StringBuilder found = new StringBuilder();
        for (Direction direction : directions) {
            //does not check position 0 because we already did that in the calling method
            for (int pos = 1; pos < word.length(); pos++) {
                //calculates the position coordinates by multiplying the directions coordinates by the
                //position count and adding the result to the starting coordinates
                int posX = x + (direction.x * (pos));
                int posY = y + (direction.y * (pos));

                //if the position is out of bounds, break the loop
                if (posX < 0 || posY < 0 || posX >= grid[0].length || posY >= grid.length)  {
                    break;
                }

                //if the character found does not match, we don't need to go in this direction anymore
                if (grid[posX][posY] != word.charAt(pos)) {
                    break;
                }

                //if the loop made it to the end of the word and the last char matches, we found the word!
                if(pos == word.length() - 1) {
                    found.append(" ").append(x).append(":").append(y).append(" ").append(posX).append(":").append(posY);
                }
            }
        }
        //return the positions if there are any
        return found;
    }

    //this class will be used to store the directions that will be traveled in the grid
    static class Direction {
        int x;
        int y;

        Direction(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
}
