package main.java.com.solution;
import org.junit.Assert;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

class DriverTest {
    String filePath = "/tmp/alphabet-soup-test.txt";

    @org.junit.jupiter.api.Test
    public void givenInput() throws IOException {
        File file = new File(filePath);
        FileWriter writer = new FileWriter(filePath);
        writer.write(
                "5x5\n" +
                        "H A S D F\n" +
                        "G E Y B H\n" +
                        "J K L Z X\n" +
                        "C V B L N\n" +
                        "G O O D O\n" +
                        "HELLO\n" +
                        "GOOD\n" +
                        "BYE"
        );
        writer.close();
        String expected =
                "HELLO 0:0 4:4\n" +
                        "GOOD 4:0 4:3\n" +
                        "BYE 1:3 1:1";
        Assert.assertEquals(expected, Driver.runOnFile(file));
        if(!file.delete()) {
            throw new RuntimeException("could not delete file");
        }
    }

    @org.junit.jupiter.api.Test
    public void noneExist() throws IOException {
        File file = new File(filePath);
        FileWriter writer = new FileWriter(filePath);
        writer.write(
                "5x5\n" +
                        "N A S D F\n" +
                        "G P L K H\n" +
                        "J K L Z X\n" +
                        "C V B L N\n" +
                        "J N L D O\n" +
                        "HELLO\n" +
                        "GOOD\n" +
                        "BYE"
        );
        writer.close();
        String expected =
                "";
        Assert.assertEquals(expected, Driver.runOnFile(file));
        if(!file.delete()) {
            throw new RuntimeException("could not delete file");
        }
    }

    @org.junit.jupiter.api.Test
    public void someExist() throws IOException {
        File file = new File(filePath);
        FileWriter writer = new FileWriter(filePath);
        writer.write(
                "5x5\n" +
                        "K A S D F\n" +
                        "G E Y B H\n" +
                        "J K L Z X\n" +
                        "C V B L N\n" +
                        "G O O D O\n" +
                        "HELLO\n" +
                        "GOOD\n" +
                        "BYE"
        );
        writer.close();
        String expected =
                        "GOOD 4:0 4:3\n" +
                        "BYE 1:3 1:1";
        Assert.assertEquals(expected, Driver.runOnFile(file));
        if(!file.delete()) {
            throw new RuntimeException("could not delete file");
        }
    }

    @org.junit.jupiter.api.Test
    public void wordsInWords() throws IOException {
        File file = new File(filePath);
        FileWriter writer = new FileWriter(filePath);
        writer.write(
                "5x5\n" +
                        "H A S D F\n" +
                        "G E Y B H\n" +
                        "J K L Z X\n" +
                        "C V B L N\n" +
                        "G O O D O\n" +
                        "HASDF\n" +
                        "HAS\n" +
                        "ASDF"
        );
        writer.close();
        String expected =
                "HASDF 0:0 0:4\n" +
                        "HAS 0:0 0:2\n" +
                        "ASDF 0:1 0:4";
        Assert.assertEquals(expected, Driver.runOnFile(file));
        if(!file.delete()) {
            throw new RuntimeException("could not delete file");
        }
    }

    @org.junit.jupiter.api.Test
    public void singleLetterWord() throws IOException {
        File file = new File(filePath);
        FileWriter writer = new FileWriter(filePath);
        writer.write(
                "5x5\n" +
                        "H A S D F\n" +
                        "Q E Y B H\n" +
                        "J K L Z X\n" +
                        "C V B L N\n" +
                        "G O O D O\n" +
                        "Q"
        );
        writer.close();
        String expected =
                "Q 1:0 1:0";
        Assert.assertEquals(expected, Driver.runOnFile(file));
        if(!file.delete()) {
            throw new RuntimeException("could not delete file");
        }
    }

    @org.junit.jupiter.api.Test
    public void allDirections() throws IOException {
        File file = new File(filePath);
        FileWriter writer = new FileWriter(filePath);
        writer.write(
                "5x5\n" +
                        "H A S D F\n" +
                        "Q E Y B H\n" +
                        "J K L Z X\n" +
                        "C V B L N\n" +
                        "G O O D O\n" +
                        "LZX\n" +
                        "LLO\n" +
                        "LBO\n" +
                        "LVG\n" +
                        "LKJ\n" +
                        "LEH\n" +
                        "LYS\n" +
                        "LBF\n"
        );
        writer.close();
        String expected =
                "LZX 2:2 2:4\n" +
                        "LLO 2:2 4:4\n" +
                        "LBO 2:2 4:2\n" +
                        "LVG 2:2 4:0\n" +
                        "LKJ 2:2 2:0\n" +
                        "LEH 2:2 0:0\n" +
                        "LYS 2:2 0:2\n" +
                        "LBF 2:2 0:4";
        Assert.assertEquals(expected, Driver.runOnFile(file));
        if(!file.delete()) {
            throw new RuntimeException("could not delete file");
        }
    }

    @org.junit.jupiter.api.Test
    public void tinyGrid() throws IOException {
        File file = new File(filePath);
        FileWriter writer = new FileWriter(filePath);
        writer.write(
                "1x1\n" +
                        "H\n" +
                        "H"
        );
        writer.close();
        String expected =
                "H 0:0 0:0";
        Assert.assertEquals(expected, Driver.runOnFile(file));
        if(!file.delete()) {
            throw new RuntimeException("could not delete file");
        }
    }

    @org.junit.jupiter.api.Test
    public void wordsWithSpaces() throws IOException {
        File file = new File(filePath);
        FileWriter writer = new FileWriter(filePath);
        writer.write(
                "5x5\n" +
                        "H A S D F\n" +
                        "Q E Y B H\n" +
                        "J K L Z X\n" +
                        "C V B L N\n" +
                        "G O O D O\n" +
                        "B LYS\n" +
                        "H Q JC G"
        );
        writer.close();
        String expected =
                "BLYS 3:2 0:2\n" +
                "HQJCG 0:0 4:0";
        Assert.assertEquals(expected, Driver.runOnFile(file));
        if(!file.delete()) {
            throw new RuntimeException("could not delete file");
        }
    }
}